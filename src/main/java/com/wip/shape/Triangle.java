/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shape;

/**
 *
 * @author WIP
 */
public class Triangle {
    double h,b;
    public Triangle(double b, double h){
        this.b = b;
        this.h = h;
    }
    public double calTriangle(){
        return 0.5 * (b*h);
    }
    public void setB(double b){
        if(b<=0){
            System.out.println("Error: Base must more than zero");
            return;
        }
        this.b = b;
    }
    public void setH(double h){
        if(h<=0){
            System.out.println("Error: Heigh must more than zero");
            return;
        }
        this.h = h;
    }
    public double getB(){
        return this.b = b;
    }
    public double getH(){
        return this.h = h;
    }
}
