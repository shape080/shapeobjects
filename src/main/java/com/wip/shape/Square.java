/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shape;

/**
 *
 * @author WIP
 */
public class Square {
    double s;
    public Square(double s){
        this.s = s;
    }
    public double calSquare(){
        return s*s;
    }
    public void setS(double s){
        if(s<=0){
            System.out.println("Error: square must more than zero");
            return;
        }
        this.s = s;
    }
    public double getS(){
        return this.s = s;
    }
}
