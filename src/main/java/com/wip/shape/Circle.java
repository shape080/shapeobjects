/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shape;

/**
 *
 * @author WIP
 */
public class Circle {
    double r;
    static final double pi = 22.0/7;
    public Circle(double r){
        this.r = r;
    }
    public double calArea(){
        return pi * r * r;
    }
    public double getR(){
        return this.r;
    }
    public void setR(double r){
        if(r<=0) {
            System.out.println("Error: Raduis must more than zero!!");
            return;
        }
        this.r = r;
    }
}
