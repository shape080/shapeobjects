/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shape;

/**
 *
 * @author WIP
 */
public class Rectangle {
    double w, h;
    public Rectangle(double w, double h){
        this.w = w;
        this.h = h;
    }
    public double calRectangle(){
        return w * h;
    }
    public void setW(double w){
        if(w<=0){
            System.out.println("Error: wide must more than 0");
            return;
        }
        this.w = w;
    }
    public void setH(double h){
        if(h<=0){
            System.out.println("Error: long must more than 0");
            return;
        }
        this.h = h;
    }
    public double getW(){
        return this.w;
    }
    public double getH(){
        return this.h;
    }
}
